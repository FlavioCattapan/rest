package br.com.flapanq.loja.modelo;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

public class Projeto {
	
	private String nome;
	
	private long id;
	
	private int anoInicio;
	
	public Projeto(long id, String nome, int anoInicio) {
		super();
		this.id = id;
		this.nome = nome;
		this.anoInicio = anoInicio;
	}

	public Projeto() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public int getAnoInicio() {
		return anoInicio;
	}
	
	
	public String toXML(){
		return new XStream().toXML(this);
		
	}
	
	public String toJSON(){
		return new Gson().toJson(this);
				
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setAnoInicio(int anoInicio) {
		this.anoInicio = anoInicio;
	}
	
	

}
