package br.com.flapanq.loja.resource;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.flapanq.loja.dao.CarrinhoDAO;
import br.com.flapanq.loja.modelo.Carrinho;
import br.com.flapanq.loja.modelo.Produto;

// uri de acesso
@Path("carrinhos")
//@Produces(MediaType.APPLICATION_XML)
//@Consumes(MediaType.APPLICATION_XML)
public class CarrinhoResource {

	// parametros passado na url
	// tem a barra implicita
	@Path("{id}")
	// acessado por get
	@GET
	// estamos produzindo xml
	@Produces(MediaType.APPLICATION_XML)
	// @Produces(MediaType.APPLICATION_JSON)
	// @QueryParam identifica recursos diferentes podendo dar problema de cash
	// indepotente n�o altera nada no servidor
	public Response busca(@PathParam("id") long id) {
		Carrinho carrinho = new CarrinhoDAO().busca(id);
		return Response.ok(carrinho).build();
	}

	// cria recurso no servidor
	// o cliente envia uma representa��o para o servidor
	@POST
	// trabalha com XML
	@Consumes(MediaType.APPLICATION_XML)
	// pode ser executado 2 vezes
	public Response adiciona(Carrinho carrinho) throws Exception {
		new CarrinhoDAO().adiciona(carrinho);
		URI uri = URI.create("/carrinhos/" + carrinho.getId());
		// Responser.created 201 Created retorna no cabe�alho
		// retorna a uri
		// todos entendem o status cod
		return Response.created(uri).build();
	}

	@Path("{id}/produtos/{produtoId}")
	// remove itens no servidor
	@DELETE
	// recebendo os parametros da url
	public Response removeProduto(@PathParam("id") long id,
			@PathParam("produtoId") long produtoId) {
		Carrinho carrinho = new CarrinhoDAO().busca(id);
		carrinho.remove(produtoId);
		// retorna c�digo 200 OK
		return Response.ok().build();
	}

	// Uri utilizada para identificar um recurso inteiro
	// @Path("{id}/produtos/{produtoId}")
	// Uri utilizada para identificar uma parte de um recurso
	@Path("{id}/produtos/{produtoId}/quantidade")
	// altera itens no servidor
	// podemos receber uma string ou um objeto com a string podemos ver o xml enviado
	@PUT
	public Response alteraProduto(Produto produto, @PathParam("id") long id,
			@PathParam("produtoId") long produtoId) {
		Carrinho carrinho = new CarrinhoDAO().busca(id);
		// troca um recurso todo
		// carrinho.troca(produto);
		// troca uma parte de um recurso
		carrinho.trocaQuantidade(produto);
		return Response.ok(carrinho).build();
	}

}
