package br.com.flapanq.loja;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static junit.framework.Assert.*;

import org.junit.Before;
import org.junit.Test;
import br.com.flapanq.loja.modelo.Projeto;

public class ProjetoTest {
	
	// obs: esse teste n�o inicializa o servidor

	private Client client;
	private WebTarget webTarget;
	
	@Before
	public void start(){
		client = ClientBuilder.newClient();
		webTarget = client.target("http://localhost:8080/");
	}

	
	@Test
	public void teste(){
		String conteudo = webTarget.path("projetos/1").request().get(String.class);
		assertTrue(conteudo.contains("<nome>Minha loja"));
	}
	
	@Test
	public void adiciona(){
		
		Projeto projeto = new Projeto();
		projeto.setAnoInicio(2015);
		projeto.setNome("TESTE NOME");
		String xml = projeto.toXML();
		Entity<String> entity = Entity.entity(xml, MediaType.APPLICATION_XML);
		// executa um post 
		Response response = webTarget.path("/projetos/").request().post(entity);
		// c�digo 201 created
		assertEquals(201, response.getStatus());
		// uri adicionada na resposta
		String location = response.getLocation().getPath();
		String conteudo = webTarget.path(location).request().get(String.class);
		assertTrue(conteudo.contains("<nome>TESTE NOME"));
		
	}
	
	
	
	
}
