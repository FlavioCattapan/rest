package br.com.flapanq.loja;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static junit.framework.Assert.*;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.flapanq.loja.modelo.Carrinho;
import br.com.flapanq.loja.modelo.Produto;

import com.thoughtworks.xstream.XStream;

public class ClienteTestXS {

	private HttpServer server;
	private Client client;
	private WebTarget target;

	@Before
	public void start() {
		server = Servidor.inicializa();
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(new LoggingFilter());
		client = ClientBuilder.newClient(clientConfig);
		target = client.target("http://localhost:8080/");
	}

	@Test
	public void testaCarrinhoLocal() {

		String conteudo = target.path("carrinhosxs/1").request()
				.get(String.class);
		// unmarshal
		Carrinho carrinho = (Carrinho) new XStream().fromXML(conteudo);
		// testa o retorno 
		assertTrue(carrinho.getRua().equals(
				"Rua Vergueiro 3185, 8 andar"));

	}

	@Test
	public void testaNovoscarrinho() {

		Carrinho carrinho = new Carrinho();
		carrinho.adiciona(new Produto(314, "MICROFONE", 37, 1));
		carrinho.setRua("Rua: Dr. Roberto Feij�.");
		carrinho.setCidade("SP");
		// marshal 
		String conteudo = carrinho.toXML();
		// preparando o post
		Entity<String> entity = Entity.entity(conteudo, MediaType.APPLICATION_XML);
        // post
		Response response = target.path("/carrinhosxs/").request()
				.post(entity);
		assertEquals(201, response.getStatus());
		String location = response.getHeaderString("Location");
		// testa a resposta
		String conteudoEsperado = client.target(location).request().get(String.class);
		assertTrue(conteudoEsperado.contains("MICROFONE"));
    
	}

	@After
	public void stop() {
		// encerra o servidor
		server.stop();

	}

}
