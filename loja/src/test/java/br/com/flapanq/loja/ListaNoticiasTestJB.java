package br.com.flapanq.loja;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.Before;
import org.junit.Test;

public class ListaNoticiasTestJB {

	private Client client;
	private WebTarget target;

	@Before
	public void start() {
		// inicializando o servidor
		// configura o cliente para debugar as requisi��es
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(new LoggingFilter());
		client = ClientBuilder.newClient(clientConfig);
		target = client.target("http://localhost:8080/");
	}

	
	@Test
	public void testaCarrinhoLocal() {

		// get j� faz o unmarchal para carrinho
		String str = target.path("SpringMVC4/views/listar-noticia").request()
				.get(String.class);
		System.out.println(str);

	}


}
