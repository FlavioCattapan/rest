package br.com.flapanq.loja;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class Servidor {

	public static void main(String[] args) throws IOException {
		inicializa();
		System.out.println("Servidor rodando");
		// enter para encerrar o servidor
		System.in.read();
		// httpServer.stop();
	}

	public static HttpServer inicializa() {
		// uri base e porta
		URI uri = URI.create("http://localhost:8080/");
		// escaneia o pacote em busca de recursos
		ResourceConfig config = new ResourceConfig()
				.packages("br.com.flapanq.loja");
		HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(uri,
				config);
		return httpServer;
	}

}
