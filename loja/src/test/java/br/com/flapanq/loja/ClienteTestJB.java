package br.com.flapanq.loja;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.flapanq.loja.modelo.Carrinho;
import br.com.flapanq.loja.modelo.Produto;

public class ClienteTestJB {

	private HttpServer server;
	private Client client;
	private WebTarget target;

	@Before
	public void start() {
		// inicializando o servidor
		server = Servidor.inicializa();
		// configura o cliente para debugar as requisi��es
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(new LoggingFilter());
		client = ClientBuilder.newClient(clientConfig);
		target = client.target("http://localhost:8080/");
	}

	@Test
	public void testaQueAConexaoComOServidorFunciona() {

		// uri base
		WebTarget webTarget = client.target("http://www.mocky.io");
		// .request faz uma requisi��o .get para
		// @Path("/v2/52aaf5deee7ba8c70329fb7d")
		// retorna uma string
		String conteudo = webTarget.path("/v2/52aaf5deee7ba8c70329fb7d")
				.request().get(String.class);
		// testa parte do xml 
		assertTrue(conteudo.contains("<rua>Rua Vergueiro 3185"));

	}

	@Test
	public void testaCarrinhoLocal() {

		// get j� faz o unmarchal para carrinho
		Carrinho carrinho = target.path("carrinhos/1").request()
				.get(Carrinho.class);
		assertTrue(carrinho.getRua().equals(
				"Rua Vergueiro 3185, 8 andar"));

	}

	@Test
	public void testaNovosCarrinhos() {

		Carrinho carrinho = new Carrinho();
		carrinho.adiciona(new Produto(314, "MICROFONE", 37, 1));
		carrinho.setRua("Rua: Dr. Roberto Feij�.");
		carrinho.setCidade("GLOR");
		// preparando a entity para o post
		Entity<Carrinho> entity = Entity.entity(carrinho, MediaType.APPLICATION_XML);
        // executa o post
		Response response = target.path("/carrinhos/").request()
				.post(entity);
		// verifica status created
		assertEquals(201, response.getStatus());
		// extrai a url location
		String location = response.getHeaderString("Location");
		// teste se carrinho foi inserido
		Carrinho carrinhoRetornado = client.target(location).request().get(Carrinho.class);
		assertEquals("MICROFONE",carrinhoRetornado.getProdutos().get(0).getNome());
    
	}

	@After
	public void stop() {
		// encerra o servidor
		server.stop();

	}

}
